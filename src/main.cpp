#include "stdio.h"
#include "json.hpp"

// https://nlohmann.github.io/json/
using json = nlohmann::json;

int main() {
  printf("Hello world!\n");
}
