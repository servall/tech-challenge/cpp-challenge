# C++ Tech Challenge

## Introduction
Thank you for your interest in joining our team here at Launchcode! This challenge is designed  to showcase your development skills and should ideally take 30 to 60 minutes.

The goal is to build a simple template string renderer, that reads file content (bonus points for stdin), and outputs to stdout. Accept data from a JSON file and replace tags in the template with the parsed data from it.

Format:

```handlebars
Hello {{name}}, my name is {{myName}}.
```

Data:

```json
{ "name": "Bob", "myName": "Jane" }
```

Output:

```
Hello Bob, my name is Jane.
```

Start as simple as you can - basic top level properties with naive replacement logic.  Bonus points for nested property access, array looping, HTML escaping, or any other fancy features you can think of. (we know these are genuinely hard to do in a short amount of time, just focus on quality of work and see how far you get)

## Restrictions
- Dont use a library like mustache or handlbars of course
- Use a modern compiler (clang, gcc or msvc), demonstrate knowledge of c++11/14/17
- Ideally, cross platform / runnable on any POSIX system (std::filesystem if you can)

## Setup
1. Start with forking this repository on gitlab.
2. We have a tiny cmake file here for you.

        mkdir build && cd build && cmake ..
        make -j
        ./main

3. You'll want to write some test cases to prove functionality. Try to do so separately from the main logic, but don't worry about bring in gtest or more formal.
